/**
 * Created by sknah on 16. 7. 6.
 */
var goURL = function goURL(url, target){
	var target = target || '_blank';
	window.open(url, target);
}
const krIndex = '한글로 검색하실 경우 정확한 단어로 검색해주셔야 \n' +
	'정확한 결과를 얻을 수 있습니다.\n\n' +
	'' +
	'한글검색기능은 다른 데이터베이스를 이용하는 것이 아닌, \n\n' +
	'' +
	'인도네시아어 데이터베이스에서 검색하신 단어를 \n' +
	'결과로 포함하는 것에 대응하는 인니어를 찾는 기능이므로,\n\n' +
	'' +
	'간혹 검색결과가 맞지않아 보이는 경우가 있습니다.\n\n' +
	'' +
	'단어를 검색하시고 리스트에서 인니어를 직접 클릭하여\n' +
	'맞는 단어인지 꼭 확인하시고 사용해주시기 바랍니다.';
var h2r = {};
var h2rPrepared = false;
var listChanged = false;
var searchHistory = [];
var searchType = 0;
var getVoca;
var originalKr = '';
$(document).ready(function(){
	var hideLoading = function(){
		setTimeout(function(){
			$('#loading').removeClass('opened');
		},500);
	}
	var vocaLength = 0;
	var offset = 0;
	var initialVocaList = function initialVocaList(limit) {
		sgAjax({limit: limit, offset: offset}, location.origin + '/vocalist', 'get', {
			success: function (data) {
				//data.result.forEach(function (voca) {
				//$scope.data.vocalist.allList.push(voca);
				let listTmp = '';
				for (var idx=0;idx<data.result.length;idx++){
					var voca = data.result[idx];
					listTmp += '<li data-id=' + voca.ID + ' onclick="getVoca(this);">' + voca.in + '</li>';
					if (idx === data.result.length -1){
						offset += limit;
						if (vocaLength > offset && !listChanged) {
							$('#vocalistE > ul').append(listTmp);
							initialVocaList(limit);
						}
					}
				}
			}
		});

	};
	var getVocaCount = function() {
		sgAjax({}, location.origin + '/getVocaCount', 'get', {
			success: function (data) {
				vocaLength = parseInt(data.length);
				initialVocaList(500);
			}
		});
	};
	var getRoman;
	(getRoman = function getRoman(offset) {
		sgAjax({limit: 100, offset: offset}, location.origin + '/getRoman', 'get', {
			success: function (data) {
				if (data.nomore > 0) {
					Object.assign(h2r, data.result);
					getRoman(offset + 100);
				} else {
					h2rPrepared = true;
					$('#romanizer button').removeClass('disabledButton');
					if (window.localStorage['initial'] === 'true') getVocaCount();
				}
			}
		});
	})(0);

	var getVocaList = function getVocaList(sqlPostfix) {
		listChanged = true;
		$('#loading').addClass('opened');
		sgAjax({where: sqlPostfix}, location.origin + '/vocalist', 'get', {
			success: function (data) {
				$('#vocalistE > ul').empty();
				let listTmp = '';
				for (var idx=0;idx<data.result.length;idx++){
					var voca = data.result[idx];
					listTmp += '<li data-id=' + voca.ID + ' onclick="getVoca(this);">' + voca.in + '</li>';
					if (idx === data.result.length -1){
						$('#vocalistE > ul').append(listTmp);
						hideLoading();
					}
				}

			}
		});
	};

	var getVocaMean = function (input, type) {
		$('#rt, #en, #kr').val('');
		originalKr = '';
		var sqlPostfix = "";
		switch (type) {
			case 'id' :
				sqlPostfix = "`ID`=\"" + input + "\"";
				break;
			case 'in' :
				sqlPostfix = "`in`=\"" + input + "\"";
				break;
			case 'en' :
				sqlPostfix = "`en` LIKE \"" + input + "\" or `en` LIKE \"" + input + ", %%\" or `en` LIKE \"%% " + input + ",\" or `en` LIKE \"%% " + input + ",%%\" or `en` LIKE \"%% " + input + "\";";
				getVocaList(sqlPostfix);
				return;
				break;
			case 'kr' :
				sqlPostfix = "`kr` LIKE \"%%" + input + "%%\";";
				getVocaList(sqlPostfix);
				return;
				break;
			case 'rt' :
				sqlPostfix = "`rt` LIKE \"" + input + "\" or `rt` LIKE \"" + input + ", %%\" or `rt` LIKE \"%% " + input + ",\" or `rt` LIKE \"%% " + input + ",%%\" or `rt` LIKE \"%% " + input + "\";";
				getVocaList(sqlPostfix);
				return;
				break;
			default :
				return;
				break;
		}
		$('#loading').addClass('opened');
		sgAjax({sqlpostfix: sqlPostfix, type: 'all'}, location.origin + '/specvoca', 'get', {
			success: function (data) {
				if (data.length > 0) {
					$('#rt').val(data.result[0].rt);
					$('#en').val(data.result[0].en);
					$('#kr').val(data.result[0].kr);
					$('#result').addClass('opened');
				} else {
					alert('검색결과가 없습니다.');
				}
				hideLoading();
			}
		});
	};

	if (window.localStorage['initial'] === 'true') {
		$('#another').html('전체단어보지않기');
	} else {
		$('#another').html('전체단어보기');
	}

	new formplate({selector: '.form-el'});
	var height = 0;
	$('#kr').parent().siblings().each(function () {
		height += $(this).height();
	});
	$('#kr').parent().css({
		height: 'calc(100% - ' + height + 'px)'
	});
	$('#kr').css({
		height: 'calc(100% - ' + ($('#kr').prev().height() + 34) + 'px)',
		marginBottom: 0,
		transitionDuration: 0
	});
	$('#result').css({transition: 'all 1s'});
	$('#fog').css({transition: 'opacity 1s'});
	$('#searchType .dropdown li').click(function (e) {
		$('#rt, #en, #kr').val('');
		console.log(e);
		if (e.target.innerText === '한국어') {
			$('#kr').val(krIndex);
		}
	});

	$('#searchType input').on('keydown', function (event) {
		if (event.keyCode === 40) {
			var lis = $(this).parent().parent().next().find('li');
			var i;
			for (i = 0; i < lis.length; i++) {
				if (lis[i].innerText === this.value) break;
			}
			if (i === lis.length - 1) return;
			$('#rt, #en, #kr').val('');
			searchType = i + 1;
			$(this).val(lis[i + 1].innerText);
			lis.removeClass('hidden');
			$(lis[i + 1]).addClass('hidden');
			if (lis[i + 1].innerText === '한국어') {
				$('#kr').val(krIndex);
			}
		} else if (event.keyCode === 38) {
			var lis = $(this).parent().parent().next().find('li');

			var i;
			for (i = 0; i < lis.length; i++) {
				if (lis[i].innerText === this.value) break;
			}
			if (i === 0) return;
			$('#rt, #en, #kr').val('');
			searchType = i - 1;
			$(this).val(lis[i - 1].innerText);
			lis.removeClass('hidden');
			$(lis[i - 1]).addClass('hidden');
			if (lis[i - 1].innerText === '한국어') {
				$('#kr').val(krIndex);
			}
		}
	});
	$('.head #searchType').click(function(e){
		$('.head #searchType .dropdown').slideToggle();
	});
	$('.head #searchType').bind("clickoutside", function () {
		$('.head #searchType .dropdown').slideUp();
		$('.head #searchType i').removeClass('active')
	});
	$('.head #searchType li').click(function(e){
		$(this).siblings().removeClass('hidden');
		$(this).addClass('hidden');
		$('.head #searchType input').val($(this).text());
	});
	$('.head #searchInput i').click(function(e){
		$('.head #searchInput .dropdown').slideToggle();
	});
	getVoca = function getVoca(self){
		var id = self.getAttribute('data-id');
		getVocaMean(id, 'id');
		$('#vocalistE > ul > li.active').removeClass('active');
		$(self).addClass('active');
	}
	$('.head #searchSubmit').click(function(e){
		var inputVal = $('.head #searchInput input').val();
		if (!inputVal || inputVal.replace(/ /g, '') === '') return;
		if (searchHistory.indexOf(inputVal) < 0) {
			searchHistory.push(inputVal);
		}
		var type = "in";
		switch(searchType) {
			case 1 :
				type = 'kr';
				break;
			case 2 :
				type = 'en';
				break;
			case 3 :
				type = 'rt';
				break;
			case 0 :
			default :
				break;
		}
		getVocaMean(inputVal, type);
	});
	$('.head #another').click(function(e){
		var win = window.open('', '_self'); win.close()
	});


	var ignore = function (event) {
		if ((event.keyCode === 45 && event.ctrlKey === true) ||
			(event.keyCode === 67 && event.ctrlKey === true)) {
		} else if (event.keyCode === 65 && event.ctrlKey === true) {
			event.target.setSelectionRange(0, event.target.value.length);
		} else {
			event.preventDefault();
		}
	};
	$('#result input, #result textarea').keyup(ignore);
	$('#result input, #result textarea').keydown(ignore);
	$('#result input, #result textarea').keypress(ignore);
	$('.head #searchInput').keypress(function(e){
		if (e.keyCode === 13) {
			$('.head #searchSubmit').click();
		}
	});


	$('#information button').click(function(){
		$('#fog').animate({opacity: 1}, {
			duration: 500, start: function () {
				$('#fog').css({
					display: 'block'
				});
			}
		});
		$('#fog > #informationDialog').toggleClass('opened');
	});
	$('#romanizer button').click(function(){
		if (originalKr !== '') {
			$('#kr').val(originalKr);
			originalKr = '';
		}else {
			originalKr = $('#kr').val();
			var romanized = '';
			for (var i = 0; i < originalKr.length; i++) {
				romanized += h2r[originalKr[i]] || originalKr[i];
			}
			$('#kr').val(romanized);
		}
	})
	$('#closeButton').click(function(){
		$('#fog').css({
			opacity: 0,
			display: 'none'
		});
		$('#fog > #informationDialog').toggleClass('opened');
	});
	$('#another').click(function(e){
		if (window.localStorage['initial'] === 'true') {
			window.localStorage['initial'] =  'false';
			e.target.innerHTML = '전체단어보기';
			listChanged = true;
			$('#vocalistE > ul').empty();
		} else {
			var r = confirm('전체단어보기는 데이터소모가 많을 수 있습니다. 계속 하시겠습니까?');
			if (r === true) {
				window.localStorage['initial'] =  'true';
				e.target.innerHTML = '전체단어보지않기';
				listChanged = false;
				getVocaCount();
			}
		}
	});

	var doResize;
	(doResize = function doResize(){
		var InputForm = $('.head #searchType input')[0].getBoundingClientRect();
		var length = $('.head #searchType .dropdown ul li').length;
		$('.head #searchType .dropdown').css({
			top: (InputForm.top + InputForm.height) + 'px',
			left: InputForm.left + 'px',
			width: InputForm.width + 'px',
			height: (((length - 1) * 25) < 200 ? ((length - 1) * 25 + 1) : 200) + 'px'
		});
		InputForm = $('.head #searchInput input')[0].getBoundingClientRect();
		length = $('.head #searchType .dropdown ul li').length;
		$('.head #searchInput .dropdown').css({
			top: (InputForm.top + InputForm.height) + 'px',
			left: InputForm.left + 'px',
			width: InputForm.width + 'px',
			height: ((length * 25) < 200 ? (length * 25 + 1) : 200) + 'px'
		})
	})();

	$(window).resize(function () {
		doResize();
	});
	setTimeout(function(){
		$('#else > iframe').animate({width:'100%', height:'100%',marginLeft:0});
	},1000);
});
