/**
 * Created by sknah on 16. 7. 6.
 */

var setNewData = function setNewData(oldobject, newobject) {
	for(var key in newobject) {
		oldobject[key] = newobject[key];
	}
};
var languageSet = {
	ko_KR: {
		root: '어근',
		korean: '한국어',
		english: '영어',
		indonesian: '인니어',
		search: '검색',
		wholevoca: '전체단어보기',
		dontwholevoca: '전체단어보지않기',
		information: '정보',
		romanize: '로마자변환',
		donate: '개발자후원',
		addvoca: '새단어등록',
		inputholder: '검색어를 입력해주세요',
		confirmwholevoca: '전체단어보기는 데이터소모가 많을 수 있습니다.\n계속 하시겠습니까?',
		nodata:'검색결과가 없습니다.\n입력을 확인해보세요.',
		krIndex: '한글로 검색하실 경우 정확한 단어로 검색해주셔야<br>' +
		'정확한 결과를 얻을 수 있습니다.<br><br>' +
		'한글검색기능은 다른 데이터베이스를 이용하는 것이 아닌, <br><br>' +
		'인도네시아어 데이터베이스에서 검색하신 단어를 <br>' +
		'결과로 포함하는 것에 대응하는 인니어를 찾는 기능이므로,<br><br>' +
		'간혹 검색결과가 맞지않아 보이는 경우가 있습니다.<br><br>' +
		'단어를 검색하시고 리스트에서 인니어를 직접 클릭하여<br>' +
		'맞는 단어인지 꼭 확인하시고 사용해주시기 바랍니다.'
	},
	en_US: {
		root: 'Radix',
		korean: 'Korean',
		english: 'English',
		indonesian: 'Indonesian',
		search: 'Search',
		wholevoca: 'Show word list',
		dontwholevoca: 'Hide word list',
		information: 'Information',
		romanize: 'Convert to alphabet',
		donate: 'Donate',
		addvoca: 'Add new word',
		inputholder: 'Search word',
		confirmwholevoca: 'Be careful. "Show whole word list" may spend large data.\nDo you continue?',
		nodata:'No Search Result.\nPlease check your input datas.',
		krIndex: 'In order to get accurate results,<br>' +
			'You should use exact word.<br><br>' +
			'Sometimes search results are seems wrong and too many.<br><br>' +
			'So before you use the result,<br>' +
			'please click word on list and check.'
	},
	in_ID: {
		root: 'Kata dasar',
		korean: 'Bhs Korea',
		english: 'Bhs Inggris',
		indonesian: 'Bhs Indonesia',
		search: 'Cari',
		wholevoca: 'Lihat kata tabel',
		dontwholevoca: 'Sembunyi kata tabel',
		information: 'Informasi',
		romanize: 'Dikonversi ke alphabet',
		donate: 'Donasi',
		addvoca: 'Tambah kata baru',
		inputholder: 'Cari kata',
		confirmwholevoca: 'Perhatian. "Lihat kata semua tabel" bisa pakai banyak data.\nDo you continue?',
		nodata:'Tidak ada hasil.\nTolong cek masuk data.',
		krIndex: 'Untuk dapat hasil lebih benar,<br>' +
			'Harus pakai kata yang tepat.<br><br>' +
			'Kadang-kadang kelihatan hasil itu ada masalah atau terlalu banyak.<br><br>' +
			'Jadi sebelum pakai hasil,<br>' +
			'tolong cek kata yang anda klik.'
	}
};
var languageKey = window.localStorage['languageKey'] ? window.localStorage['languageKey'] : 'en_US';
window.localStorage['languageKey'] = languageKey;
var language = {};
setNewData(language, languageSet[languageKey]);
language = JSON.parse(JSON.stringify(languageSet[languageKey]));
new Vue({
	el: 'body',
	data: language
});
