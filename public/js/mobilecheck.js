/**
 * Created by sknah on 16. 7. 6.
 */
var mobile = (/iphone|ipad|ipod|android/i.test(navigator.userAgent.toLowerCase()));
if (mobile) {
	if(/\/m/i.test(location.pathname) === false) {
		location.href = '/m';
	}
} else {
	if(/\/pc/i.test(location.pathname) === false) {
		if (history.length > 1) {
			window.open('/pc', '_blank', 'width=800px, height=600px, location=no, status=no, toolbar=no, menubar=no');
			history.back();
		} else {
			location.href = '/pc';
		}

	}
}
