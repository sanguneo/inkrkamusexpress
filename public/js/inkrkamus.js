/**
 * Created by sknah on 16. 7. 6.
 */

var goURL = function goURL(url, target){
	var target = target || '_blank';
	window.open(url, target);
};
var currentOS;
var isMobile = false;
var mobile = (/iphone|ipad|ipod|android/i.test(navigator.userAgent.toLowerCase()));
if (mobile) {
	if(/\/m/i.test(location.pathname) === false) {
		location.href = '/m';
	}
	isMobile = true;
	var userAgent = navigator.userAgent.toLowerCase();
}
var setInput = function setInput(li){
	$('#rt, #en, #kr').val('');
	$('#kr').html('');
	$('.head #searchInput input').val(li.innerHTML);
	$('#result').addClass('opened');
	$('#result.mob').fadeIn(500);
	submit();
};
var krIndex = language.krIndex;
var h2r = {};
var h2rPrepared = false;
var listChanged = false;
var searchHistory = [];
var searchType = 0;
var getVoca;
var originalKr = '';
var submit;
$(document).ready(function(){
	$('#title').click(function () {
		location.reload();
	});
	var initialWindowHeight = $(window).height();
	var hideLoading = function(){
		setTimeout(function(){
			$('#loading').removeClass('opened');
			$('.dropdown').slideUp();
			$('.head #searchType i').removeClass('active');
			$('#result input').prop("readonly", true);
		},500);
	};
	var vocaLength = 0;
	var offset = 0;
	var initialVocaList = function initialVocaList(limit) {
		sgAjax({limit: limit, offset: offset}, location.origin + '/vocalist', 'get', {
			success: function (data) {
				var listTmp = '';
				for (var idx=0;idx<data.result.length;idx++){
					var voca = data.result[idx];
					listTmp += '<li data-id=' + voca.ID + ' onclick="getVoca(this);">' + voca.in + '</li>';
					if (idx === data.result.length -1){
						offset += limit;
						if (vocaLength > offset && !listChanged) {
							$('#vocalistE ul').append(listTmp);
							initialVocaList(limit);
						}
					}
				}
			}
		});

	};
	var getVocaCount = function() {
		sgAjax({}, location.origin + '/getVocaCount', 'get', {
			success: function (data) {
				vocaLength = parseInt(data.length);
				$('#vocalistE ul').empty();
				initialVocaList(500);
			}
		});
	};
	var getRoman;
	(getRoman = function getRoman(offset) {
		if (window.localStorage['h2r']){
			h2r = JSON.parse(window.localStorage['h2r']);
			if (typeof h2r === 'string'){
				window.localStorage.removeItem('h2r');
				getRoman(offset);
			}
			h2rPrepared = true;
			$('#romanizer button').removeClass('disabledButton');
			$('#romanizer').removeClass('disabled');
			if (window.localStorage['initial'] === 'true') getVocaCount();
		} else {
			sgAjax({limit: 1000, offset: offset}, location.origin + '/getRoman', 'get', {
				success: function (data) {
					if (data.nomore > 0) {
						Object.assign(h2r, data.result);
						getRoman(offset + 1000);
					} else {
						h2rPrepared = true;
						window.localStorage['h2r'] = JSON.stringify(h2r);
						$('#romanizer button').removeClass('disabledButton');
						$('#romanizer').removeClass('disabled');
						if (window.localStorage['initial'] === 'true') getVocaCount();
					}
				}
			});
		}
	})(0);
	var hideKeyboard = function hideKeyboard(element) {
		element.attr('readonly', 'readonly'); // Force keyboard to hide on input field.
		element.attr('disabled', 'true'); // Force keyboard to hide on textarea field.
		setTimeout(function() {
			element.blur();  //actually close the keyboard
			// Remove readonly attribute after keyboard is hidden.
			element.removeAttr('readonly');
			element.removeAttr('disabled');
		}, 100);
	};
	var getVocaList = function getVocaList(sqlPostfix) {
		$('#kr').parent().siblings().fadeIn(500);
		$('#kr').siblings().fadeIn(500);
		hideKeyboard($('input'));
		listChanged = true;
		$('#loading').addClass('opened');
		$('#result').focus();
		sgAjax({where: sqlPostfix, input: $('.head #searchInput input').val()}, location.origin + '/vocalist', 'get', {
			success: function (data) {
				$('#vocalistE ul').empty();
				if(data.result.length <=0) {
					alert(language.nodata);
					$('#result').removeClass('opened');
					$('#result.mob').fadeOut(500);
					hideLoading();
				}
				var listTmp = '';
				var frontListTmp = '';
				for (var idx=0;idx<data.result.length;idx++){
					var voca = data.result[idx];
					if (voca.in.startsWith($('.head #searchInput input').val())){
						frontListTmp += '<li data-id=' + voca.ID + ' onclick="getVoca(this);">' + voca.in + '</li>';
					}else {
						listTmp += '<li data-id=' + voca.ID + ' onclick="getVoca(this);">' + voca.in + '</li>';
					}
					if (idx === data.result.length -1){
						listTmp = frontListTmp + listTmp;
						$('#vocalistE ul').append(listTmp);
						if ($('#vocalistE ul li:first-child').text() === $('.head #searchInput input').val()) {
							getVoca($('#vocalistE ul li:first-child')[0]);
						} else {
							$('#result.mob').fadeOut(500);
						}
						hideLoading();
					}
				}
			}, error: function (error) {
				console.error(language.nodata, error);
				$('#result.mob').fadeOut(500);
				hideLoading();
			}
		});
	};

	var getVocaMean = function (input, type) {
		$('#rt, #en').val('');
		$('#kr').html('');
		originalKr = '';
		var sqlPostfix = "";
		switch (type) {
			case 'id' :
				sqlPostfix = "`ID`=\"" + input + "\"";
				break;
			case 'in' :
				sqlPostfix = "`in` LIKE \"" + input + "\" or `in` LIKE \"%% " + input + "\";";
				getVocaList(sqlPostfix);
				return;
				break;
			case 'en' :
				sqlPostfix = "`en` LIKE \"" + input + "\" or `en` LIKE \"" + input + ", %%\" or `en` LIKE \"%% " + input + ",\" or `en` LIKE \"%% " + input + ",%%\" or `en` LIKE \"%% " + input + "\";";
				getVocaList(sqlPostfix);
				return;
				break;
			case 'kr' :
				sqlPostfix = "`kr` like \"%%" + input + "%%\" " +
					"and `kr` not like \"%%" + input + "이%%\" " +
					"and `kr` not like \"%%" + input + "가%%\" " +
					"and `kr` not like \"%%" + input + "은%%\" " +
					"and `kr` not like \"%%" + input + "는%%\" " +
					"and `kr` not like \"%%" + input + "들%%\" " +
					"and `kr` not like \"%%" + input + "을%%\" " +
					";";
				getVocaList(sqlPostfix);
				return;
				break;
			case 'rt' :
				sqlPostfix = "`rt` LIKE \"" + input + "\" or `rt` LIKE \"" + input + ", %%\" or `rt` LIKE \"%% " + input + ",\" or `rt` LIKE \"%% " + input + ",%%\" or `rt` LIKE \"%% " + input + "\";";
				getVocaList(sqlPostfix);
				return;
				break;
			default :
				return;
				break;
		}
		$('#loading').addClass('opened');
		sgAjax({sqlpostfix: sqlPostfix, type: 'all'}, location.origin + '/specvoca', 'get', {
			success: function (data) {
				if (data.length > 0) {
					hideKeyboard($('input'));
					$('#rt').val(data.result[0].rt);
					$('#en').val(data.result[0].en);
					$('#kr').html(data.result[0].kr.replace(/\r\n/g,'<br>'));
					$('#result.mob').fadeIn(500);
				} else {
					alert(language.nodata);
					$('#result.mob').fadeOut(500);
				}
				hideLoading();
			}
		});
	};

	if (window.localStorage['initial'] === 'true') {
		$('#another').html(language.dontwholevoca);
	} else {
		$('#another').html(language.wholevoca);
	}

	$('#result').css({transition: 'all 1s'});
	$('#fog').css({transition: 'opacity 1s'});

	$('#searchType input').on('keydown', function (event) {
		if (event.keyCode === 40) {
			var lis = $(this).parent().parent().next().find('li');
			var i;
			for (i = 0; i < lis.length; i++) {
				if (lis[i].innerText === this.value) break;
			}
			if (i === lis.length - 1) return;
			$('#rt, #en').val('');
			$('#kr').val('');
			searchType = i + 1;
			$(this).val(lis[i + 1].innerText);
			lis.removeClass('hidden');
			$(lis[i + 1]).addClass('hidden');
			if (lis[i + 1].innerText === language.korean) {
				$('#kr').html(krIndex.replace(/\r\n/g,'<br>'));
			}
		} else if (event.keyCode === 38) {
			var lis = $(this).parent().parent().next().find('li');
			var i;
			for (i = 0; i < lis.length; i++) {
				if (lis[i].innerText === this.value) break;
			}
			if (i === 0) return;
			$('#rt, #en').val('');
			$('#kr').val('');
			searchType = i - 1;
			$(this).val(lis[i - 1].innerText);
			lis.removeClass('hidden');
			$(lis[i - 1]).addClass('hidden');
			if (lis[i - 1].innerText === language.korean) {
				$('#kr').html(krIndex.replace(/\r\n/g,'<br>'));
			}
		}
	});
	$('.head #searchType').click(function(e){
		$('.head #searchType i').toggleClass('active');
		$('.head #searchType .dropdown').slideToggle();
	});
	$('.head #searchType').bind("clickoutside", function () {
		$('.head #searchType i').removeClass('active');
		$('.head #searchType .dropdown').slideUp();
	});
	$('.head #searchType li').click(function(e){
		$('#rt, #en, #kr').val('');
		$('#kr').html('');
		searchType = parseInt(e.target.getAttribute('data-idx'));
		if (e.target.innerText === language.korean) {
			$('#kr').html(krIndex.replace(/\r\n/g,'<br>'));
			$('#kr').parent().siblings().fadeOut(500);
			$('#kr').siblings().fadeOut(500);
		}
		$(this).siblings().removeClass('hidden');
		$(this).addClass('hidden');
		$('.head #searchType input').val($(this).text());
		$('#result').addClass('opened');
		$('#result.mob').fadeIn(500);
		if($('.head #searchInput input').val() !== '')
			submit();
	});


	$('.head #searchInput i').click(function(e){
		$('.head #searchInput .dropdown').slideToggle();
	});
	getVoca = function getVoca(self){
		var id = self.getAttribute('data-id');
		getVocaMean(id, 'id');
		$('#vocalistE ul > li.active').removeClass('active');
		$(self).addClass('active');
	};
	submit = function submit(){
		var inputVal = $('.head #searchInput input').val();
		if (!inputVal) return;
		if (inputVal.replace(/ /g, '') === '') return;
		if (searchHistory.indexOf(inputVal) < 0) {
			searchHistory.push(inputVal);
			$('#searchInput .dropdown ul').append('<li onclick="setInput(this);">' + inputVal + '</li>');
		}
		var type = "in";
		switch(searchType) {
			case 1 :
				type = 'kr';
				break;
			case 2 :
				type = 'en';
				break;
			case 3 :
				type = 'rt';
				break;
			case 0 :
			default :
				break;
		}
		getVocaMean(inputVal, type);
	};
	$('.head #searchSubmit').click(function(e){
		submit();
	});
	//$('.head #another').click(function(e){
	//	var win = window.open('', '_self'); win.close()
	//});


	var ignore = function (event) {
		if ((event.keyCode === 45 && event.ctrlKey === true) ||
			(event.keyCode === 67 && event.ctrlKey === true)) {
		} else if (event.keyCode === 65 && event.ctrlKey === true) {
			event.target.setSelectionRange(0, event.target.value.length);
		} else {
			event.preventDefault();
		}
	};
	$('#result input').prop("readonly", true);
	//if (isMobile){
	//	$('#result input, #result textarea').prop("readonly", true);
	//} else {
		$('#result textarea').keyup(ignore);
		$('#result textarea').keydown(ignore);
		$('#result textarea').keypress(ignore);
	//}
	$('.head #searchInput').keypress(function(e){
		if (e.keyCode === 13) {
			$('.head #searchSubmit').click();
		}
	});


	$('#information').click(function(){
		$('#fog').animate({opacity: 1}, {
			duration: 500, start: function () {
				$('#fog').css({
					display: 'block'
				});
			}
		});
		$('#fog > #informationDialog').toggleClass('opened');
	});
	$('#romanizer').click(function(){
		if (originalKr !== '') {
			$('#kr').html(originalKr);
			originalKr = '';
		}else {
			originalKr = $('#kr').html();
			var romanized = '';
			for (var i = 0; i < originalKr.length; i++) {
				romanized += h2r[originalKr[i]] || originalKr[i];
			}
			$('#kr').html(romanized);
		}
	});
	$('#closeButton').click(function(){
		$('#fog').css({
			opacity: 0,
			display: 'none'
		});
		$('#fog > #informationDialog').toggleClass('opened');
	});
	$('#another').click(function(e){
		if (window.localStorage['initial'] === 'true') {
			window.localStorage['initial'] =  'false';
			e.target.innerHTML = language.wholevoca;
			listChanged = true;
			setTimeout(function(){
				$('#vocalistE ul').empty();
			},1000);
		} else {
			var r = confirm(language.confirmwholevoca);
			if (r === true) {
				window.localStorage['initial'] =  'true';
				e.target.innerHTML = language.dontwholevoca;
				listChanged = false;
				offset = 0;
				$('#vocalistE ul').empty();
				getVocaCount();
			}
		}
	});
	$('#languageButton').click(function (e) {
		$('#languageSelect').fadeIn(500);
	});
	$('#languageSelect').click(function (e) {
		if (e.target.id === 'languageSelect') {
			$('#languageSelect').fadeOut(500);
		}
	});
	$('#resultClose').click(function (e) {
		$('#result.mob').fadeOut(500);
	});
	$('#languageSelect > ul > li').click(function (e) {
		switch (e.target.innerText) {
			case '한국어' :
				window.localStorage['languageKey'] = 'ko_KR';
				break;
			case 'Bahasa Indonesia' :
				window.localStorage['languageKey'] = 'in_ID';
				break;
			default :
				window.localStorage['languageKey'] = 'en_US';
				break;
		}
		languageKey = window.localStorage['languageKey'] ? window.localStorage['languageKey'] : 'en_US';
		setNewData(language, languageSet[languageKey]);
		$('#languageSelect').fadeOut(500);
	});

	var doResize;
	if (isMobile) {

		doResize = function doResize() {
			var InputForm = $('.head #searchType input')[0].getBoundingClientRect();
			var length = $('.head #searchType .dropdown ul li').length;
			$('.head #searchType .dropdown').css({
				// top: (InputForm.top + InputForm.height) + 'px',
				// left: InputForm.left + 'px',
				// width: InputForm.width + 'px',
				height: (((length - 1) * 25) < 200 ? ((length - 1) * 25 + 1) : 200) + 'px'
			});
			InputForm = $('.head #searchInput input')[0].getBoundingClientRect();
			length = $('.head #searchType .dropdown ul li').length;
			$('.head #searchInput .dropdown').css({
				//top: (InputForm.top + InputForm.height) + 'px',
				//left: InputForm.left + 'px',
				//width: InputForm.width + 'px',
				height: ((length * 25) < 200 ? (length * 25 + 1) : 200) + 'px'
			})
		};
	} else {
		doResize = function doResize() {
			var InputForm = $('.head #searchType input')[0].getBoundingClientRect();
			var length = $('.head #searchType .dropdown ul li').length;
			$('.head #searchType .dropdown').css({
				top: (InputForm.top + InputForm.height) + 'px',
				left: InputForm.left + 'px',
				width: InputForm.width + 'px',
				height: (((length - 1) * 25) < 200 ? ((length - 1) * 25 + 1) : 200) + 'px'
			});
			InputForm = $('.head #searchInput input')[0].getBoundingClientRect();
			length = $('.head #searchType .dropdown ul li').length;
			$('.head #searchInput .dropdown').css({
				top: (InputForm.top + InputForm.height) + 'px',
				left: InputForm.left + 'px',
				width: InputForm.width + 'px',
				height: ((length * 25) < 200 ? (length * 25 + 1) : 200) + 'px'
			})
		};
	}
	doResize();
	$(window).resize(function () {
		doResize();
	});
});
