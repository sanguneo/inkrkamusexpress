/**
 * Created by sknah on 16. 6. 13.
 */


var sgAjax = function(data,url, method, callbacks) {

	this.ajaxObject = {};
	this.ajaxObject.url = url || location.origin;
	this.ajaxObject.method = method || 'get';

	// case fetch function is undefined.
	if (!window.fetch) {
		// if jquery is not included. return;
		if (!$) return;
		this.ajaxObject.data = data;
		this.ajaxObject.timeout = 10000;
		for (var cbFn in callbacks) {
			this.ajaxObject[cbFn] = callbacks[cbFn];
		}
		$.ajax(this.ajaxObject);
		return;
		// case fetch function is defined.
	}else {
		// defaultCallbackFunction
		var defaultFunction = function (data) {	console.log(data);	}
		if (!callbacks) {
			callbacks = {
				success: defaultFunction, error: defaultFunction,complete: defaultFunction
			}
		}
		// callback matching.
		callbacks.success = callbacks.success || defaultFunction;
		callbacks.error = callbacks.error || defaultFunction;
		callbacks.complete = callbacks.complete || defaultFunction;

		//make query Datas
		var queryData;
		var queryString = '';
		if (this.ajaxObject.method === 'post') {
			queryData = {method: 'post', body: new FormData()};
			for (var key in data) {
				queryData.body.append(key, data[key]);
			}
		} else {
			queryData = {method: 'get'};
			queryString = '?' +
				Object.keys(data).map(function (key) {
					return encodeURIComponent(key) + '=' +
						encodeURIComponent(data[key]);
				}).join('&');
		}

		window.fetch(this.ajaxObject.url + queryString, queryData)
			.then(function (response) {
				if (response.ok) {
					return response.json().then(function (data) {
						callbacks.success(data);
					});
				} else {
					callbacks.error({
						status: response.status,
						statusText: response.statusText,
						type: response.type,
						url: response.url
					});
				}
			})
			.catch(function (error) {
				callbacks.error({
					status: 0,
					statusText: error.message,
					type: 'unknown',
					url: this.ajaxObject.url + queryString
				});
			});
	}
}
