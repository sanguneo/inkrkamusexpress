var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
        var sqlPostfix = req.query.sqlpostfix;
        var db = req.app.get('db');
        var selectColumnString = (req.query.type === 'all') ? ', `rt`,`en`,`kr`' : '';
        var sqlQuery = 'SELECT `ID`, `in`' + selectColumnString + ' from data where ' + sqlPostfix + ';'
        db.serialize(function() {
			db.all(sqlQuery, function (err, rows) {
					res.send({'code':200,'query': sqlQuery, length: rows.length, 'result':rows});
			});
        });
});

module.exports = router;
