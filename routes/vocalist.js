var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
        var db = req.app.get('db');
        var limitString = (req.query.limit && req.query.offset) ? ' limit ' + req.query.limit + ' offset ' + req.query.offset + ';' : '';
        var whereString = req.query.where ? ' where ' + req.query.where : '';
	var kr = '';
	if (req.query.where) {
		kr = req.query.where.startsWith('`kr`') ? ', `kr`' : '';
	}
        var sqlQuery = 'SELECT `ID`, `in`' + kr + ' from data' + whereString + limitString;
        db.serialize(function() {
			db.all(sqlQuery, function (err, rows) {
				if (kr !== '') {
					for (var idx = rows.length - 1; idx > 0; idx--) {
						var voca = rows[idx];
						if (voca.kr) {
							if (voca.kr.replace(/\s*\(.*?\)\s*/g, '').indexOf(req.query.input) < 0) {
								rows.splice(idx, 1);
							}
						}
					}
				}
				res.send({'code':200,'query': sqlQuery, 'result':rows});
			});
        });
});

module.exports = router;
