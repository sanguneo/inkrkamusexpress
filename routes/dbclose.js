var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
        var db = req.app.get('db');
        db.close();
        res.send({'code':200,'query': 'db.close();'});
});

module.exports = router;
