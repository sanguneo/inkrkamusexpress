var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
        var vocaLength=0;
        var db = req.app.get('db')
        db.serialize(function() {
                db.get("SELECT COALESCE(MAX(id)+1, 0) as count FROM data", function (err, row) {
                	vocaLength = parseInt(row.count);
                    res.send({'code':200,'query': "SELECT COALESCE(MAX(id)+1, 0) as count FROM data",'length':vocaLength});
                });
        });
});

module.exports = router;
