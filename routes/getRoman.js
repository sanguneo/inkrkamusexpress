var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
        var db = req.app.get('db');
        var limit = req.query.limit ? ' limit ' + req.query.limit : '';
        var offset = req.query.offset ? ' offset ' + req.query.offset : '';
        var sqlQuery = 'SELECT * FROM h2r' + limit + offset + ';';
        var h2r = {};
        var count = 0;
        db.serialize(function() {
        	db.each(sqlQuery, function (err, row) {
        		h2r[row.han] = row.rom;
                        count++;
        	}, function(err, rows) {
                        while (rows !== count) {
                                setTimeout(function(){},100);
                        }
                        res.send({'code':200,'query': sqlQuery,'result':h2r, 'nomore': count});
                });
        });
});

module.exports = router;
