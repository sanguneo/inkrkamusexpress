# Express Server

### 초기설정

> GIT에서 최초 clone했을 때는 package가 설치되어 있지 않다.<br>
> NodeJS와 NPM이 설치되어 있지 않다면 다음을 실행한다.

* **Debian / 우분투의 경우**
```
$ sudo apt-get install -y curl
$ sudo apt-get install -y node nodejs npm
```
* **RHEL / CentOS의 경우**
```
$ yum install npm
```
* **Windows 경우**
```
https://nodejs.org/en/download/ 에서 Window 용 패키지를 받는다.
```
* **NodeJS / NPM 설치 후**
```
$ npm install
```


### 실행
**config.json** 파일을 수정하여 실행포트를 바꿀 수 있다.
```
$ ./server.sh start/stop (debug)
```

