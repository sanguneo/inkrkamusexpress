/**
 * Include libraries.
 * @type {*|exports|module.exports}
 */
const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const sqlite3 = require('sqlite3').verbose();
const db =  new sqlite3.Database('./public/data/ik.wkdb');
/**
 * application register function
 *
 * @param express Instance of express.
 * @param uri application path
 */

var register = function(express, uri){
    express.use(uri === 'index' ? '/' : '/' + uri, require('./routes/' + uri));
}

// paths array.(User defined.)
var paths = [
    'index',
    'users',
    'backend',
    'vocalist',
    'specvoca',
    'getVocaCount',
    'getRoman',
    'dbclose',
]


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('db', db);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// paths apply.
for (var idx in paths){
    register(app, paths[idx]);
}
//app.use(require('./routes/backend'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

process.on('exit', function() {
	// Add shutdown logic here.
	db.close();
});

var http = require('http');
setInterval(function() {
http.get('http://inkrkamus.herokuapp.com', function(res) {
        if (res.statusCode !== 200) {
            console.log('Heroku Keep Alive Ping: Error - Status Code ' +
                res.statusCode);
        }
    }).on('error', function(err) {
        console.log('Heroku Keep Alive Ping: Error - ' + err.message);
    });
}, 30 * 60 * 1000); // load every 20 minutes
module.exports = app;
